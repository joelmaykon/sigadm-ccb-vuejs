// tests/unit/EditarIrmaoModal.spec.js
// http://iobio.io/user_guides/2019/05/20/vue-js-unit-testing-with-jest/
// Este erro abaixo é quando é usado uma variavel que não existe no data do componente 
// [Vue warn]: Avoid adding reactive properties to a Vue instance or its root $data at runtime - declare it upfront in the data option.

import { createLocalVue, shallowMount } from '@vue/test-utils';
import EditarIrmaoModal from '@/pages/Modulos/Patrimonio/Irmaos/EditarIrmao/EditarIrmaoModal.vue';
import Vuetify from "vuetify";
import Vue from 'vue';
import axios from 'axios';



describe('EditarIrmaoModal', () => {
  let wrapper;
  beforeEach(() => {
    Vue.use(Vuetify);
    wrapper = shallowMount(EditarIrmaoModal);
  });
  test('Rendenrizando os componentes visuais do editar irmao modal', () => {
    expect(shallowMount(EditarIrmaoModal).isVueInstance()).toBe(true);
    //expect(wrapper.element).toMatchSnapshot()
  });

  test('verificar se a variável dialog estar com o valor false', () => {
    //const local = wrapper.find({ local: '' })
    expect(wrapper.vm._data.dialog).toBe(false);
    //console.log(wrapper.vm._data.local)
  });
  test('testando se quando houver mudança no valor da prop "dialog_ativar", os dados são modificados pelo watch', () => {
    wrapper.setProps({
      irmao_dados: {
        id: 1,
        nome: "Samuel",
        cpf: "123456789",
        bairro: "Bela parnamirim",
        ruA: "Jaime de souza",
        numero: "123",
        complemento: "casa",
        cargo: "Engarregado Regional",
        cidade: "Parnamirim",
        telefone: "84 - 987223242",
        congregacao: "Boa Esperança",
        nascimento: "2019-07-15T15:15:26.222Z"
      }
    });
    wrapper.setProps({
      dialog_ativar: 'ativar'
    });  
    expect(wrapper.vm.nome).toBe('Samuel');
    expect(wrapper.vm.dialog).toBe(true);    
  });
});
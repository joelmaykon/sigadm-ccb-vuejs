// tests/unit/EditarIrmaoModal.spec.js
// http://iobio.io/user_guides/2019/05/20/vue-js-unit-testing-with-jest/
// Este erro abaixo é quando é usado uma variavel que não existe no data do componente 
// [Vue warn]: Avoid adding reactive properties to a Vue instance or its root $data at runtime - declare it upfront in the data option.


import { createLocalVue, shallowMount } from '@vue/test-utils';
import CadastrarCompraModal from '@/pages/Modulos/Patrimonio/Instrumento/Cadastros/CadastrarCompraModal.vue';
import Vuetify from "vuetify";
import Vue from 'vue';
// Import the store
import Store from "@/pages/Modulos/Patrimonio/Instrumento/Cadastros/store/storeCompra";
import { mapState } from 'vuex'


describe('CadastrarCompraModal', () => {
  let wrapper;
  beforeEach(() => {
    Vue.use(Vuetify);
    wrapper = shallowMount(CadastrarCompraModal);
  }
  );
  test('Rendenrizando os componentes visuais', () => {
    expect(shallowMount(CadastrarCompraModal).isVueInstance()).toBe(true);
    //expect(wrapper.element).toMatchSnapshot()
  });
  test('verificar se o dado da variável dialog estão como os definidos inicialmente', () => {
    expect(wrapper.vm._data.dialog).toBe(false);
  });
  test('Realizando o registro de uma compra: usar token obtido no postman no store ',()=>{
    Store.dispatch('SalvarCompraInstrumentos',"10000","0005","123456","Musical")
  });
});
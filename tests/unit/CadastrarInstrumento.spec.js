// tests/unit/EditarIrmaoModal.spec.js
// http://iobio.io/user_guides/2019/05/20/vue-js-unit-testing-with-jest/
// Este erro abaixo é quando é usado uma variavel que não existe no data do componente 
// [Vue warn]: Avoid adding reactive properties to a Vue instance or its root $data at runtime - declare it upfront in the data option.


import { createLocalVue, shallowMount } from '@vue/test-utils';
import CadastrarInstrumentoModal from '@/pages/Modulos/Patrimonio/Instrumento/Cadastros/CadastrarInstrumentoModal.vue';
import Vuetify from "vuetify";
import Vue from 'vue';
// Import the store
import Store from "@/pages/Modulos/Patrimonio/Instrumento/Cadastros/store/storeInstrumento";
import { mapState } from 'vuex'
import vueFilePond from "vue-filepond";

// Import plugins
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.esm.js";
import FilePondPluginImagePreview from "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.esm.js";
import FilePondPluginFileRename from "filepond-plugin-file-rename";
import swal from "sweetalert2";

// Import styles
import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css";

// Create FilePond component
const FilePond = vueFilePond(
  FilePondPluginFileValidateType,
  FilePondPluginImagePreview,
  FilePondPluginFileRename
);
const localVue = createLocalVue()
localVue.use(FilePond)


describe('CadastrarInstrumento', () => {
  let wrapper;
  beforeEach(() => {
    Vue.use(Vuetify);
    wrapper = shallowMount(CadastrarInstrumentoModal,{localVue});
  }
  );
  test('Rendenrizando os componentes visuais', () => {
    expect(shallowMount(CadastrarInstrumentoModal).isVueInstance()).toBe(true);
    //expect(wrapper.element).toMatchSnapshot()
  });
  test('verificar se o dado da variável dialog estão como os definidos inicialmente', () => {
    expect(wrapper.vm._data.dialog).toBe(false);
  });
  test('verificando o estado da variável nome que estar no store da aplicação', () => {
    expect(Store.state.nome).toBe('Clarinete');
  });
  test('Modificando o valor da variável nome que estar na store', () => {
    Store.commit('atualizarNomeInstrumento', 'Violino')
    expect(Store.state.nome).toBe('Violino');
  });
  test("Usando a Action da Store 'InserindoNomesInstrumentos' e verificando se os dados são inseridos corretamente na state 'nomesTest' através da mutation 'MudandoStateNomesTest'", () => {
    Store.dispatch('InserindoNomesInstrumentos');
    let quantidadeNomes = 0;
    Store.state.nomesTest.forEach(element => {
      //console.log(element)
      quantidadeNomes++;
    });
    expect(quantidadeNomes).toBe(3);
  })
});